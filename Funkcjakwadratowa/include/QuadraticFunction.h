#ifndef QUADRATICFUNCTION_H
#define QUADRATICFUNCTION_H

class QuadraticFunction
{
public:
    QuadraticFunction(double a,double b, double c); // constructor: a<>0
    ~QuadraticFunction(); // destructor
    double getDelta();    // returns computed value of delta
    int getRoots(double *x1,double *x2); // returns number of roots and values (if exists)
private:
    double va;
    double vb;
    double vc;
};


#endif // QUADRATICFUNCTION_H
