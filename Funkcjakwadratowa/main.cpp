#include <iostream>
#include "QuadraticFunction.h"
#include "MyFunctions.h"
#include <cstdio>

//wersja po zlaczeniu z mod2
using namespace std;

int main()
{
    double a,b,c;
    cout<<"Podaj wartość pierwszego parametru"<<endl;
    a=readNot0();
    cout<<"Podaj wartość drugiego parametru"<<endl;
    b=readNumber();
    cout<<"Podaj wartość trzeciego parametru"<<endl;
    c=readNumber();

    QuadraticFunction f(a,b,c);
    double delta=f.getDelta();
    cout<<"Delta równa się: "<<delta<<endl;


    double x1;
    double x2;
    int nr;

    nr=f.getRoots(&x1,&x2);

    if(nr==0)
    {
        printf("No roots\n");
    }
    else if(nr==1)
    {
        printf("One root: %.4lf\n",x1);
    }
    else
    {
        printf("Two roots: %.4lf and %.4lf\n",x1,x2);
    }

    return 0;
}
