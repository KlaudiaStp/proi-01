#include "MyFunctions.h"
#include <cstdio>
#include <iostream>

using namespace std;
void cleanBuf (void)
{
    char c;
    do
        c=getchar();
    while(c!='\n' && c!=EOF);

}

double readNot0()
{

    double number;
    while(1!=scanf("%lf",&number) && number!=0)
    {
        cleanBuf();
        cout<<"Niepoprawna wartość, podaj jeszcze raz"<<endl;
    }

    return number;
}

double readNumber()
{
    double number=0;
    while(1!=scanf("%lf",&number))
    {
        cleanBuf();
        cout<<"Niepoprawna wartość, podaj jeszcze raz"<<endl;
    }

    return number;

}
