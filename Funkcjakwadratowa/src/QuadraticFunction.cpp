#include "QuadraticFunction.h"
#include <math.h>

QuadraticFunction::QuadraticFunction(double a,double b, double c)
{
 // constructor: the value of "a" should be <> 0
    va=a;
    vb=b;
    vc=c;
}
QuadraticFunction::~QuadraticFunction()
{
    //destructor (empty)
}

double QuadraticFunction::getDelta()
{ // returns computed value of delta
    return vb*vb-4*va*vc;
}

int QuadraticFunction::getRoots(double *x1,double *x2)
{
    int numberOfRoots;
    double delta=getDelta();
    if (delta<0) // no roots
    {
        numberOfRoots=0;
    }
    else if(delta==0) // one root
    {
        numberOfRoots=1;
        *x1=*x2=-vb/(2*va);
    }
    else // two different roots
    {
        numberOfRoots=2;
        *x1=(-vb+sqrt(delta))/(2*va);
        *x2=(-vb-sqrt(delta))/(2*va);
    }
    return numberOfRoots;
}
